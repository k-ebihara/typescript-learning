# ECMAScript と TypeScript

<p class='small'><small>制作: 2018-06-27 海老原 賢次</small></p>

## はじめに

JavaScript は昨今では、ブラウザだけでなく、Node.jsやNodeRedでのサーバー処理やElectron、Cordovaなどのマルチプラットフォームアプリケーション開発でも利用されるなど、重要な言語の1つになっていると言える。

しかし、JavaScriptの仕様となっている ECMAScript(ES) は現在も更新中であり、過去のバージョンの仕様の中で非推奨になったものも多く、JavaScriptを使う開発者としてきちんと把握しておく必要がある。

また、JavaScriptは中～大規模な開発向けに利用できるように Alt(代替) JavaScriptとして、TypeScriptの人気が高まっている。  
[StackOverflowで愛されている言語ランキング4位](https://insights.stackoverflow.com/survey/2018#most-loved-dreaded-and-wanted)

TypeScriptはESをベースに拡張しているので、ESも合わせて習得するのが良いと考える。


### 目次

- [ECMAScript と TypeScript](#ecmascript-と-typescript)
    - [はじめに](#はじめに)
        - [目次](#目次)
    - [ES6以上 の主な変更点](#es6以上-の主な変更点)
        - [モジュール(import/export)](#モジュールimportexport)
        - [変数と定数](#変数と定数)
        - [arrow関数](#arrow関数)
        - [テンプレート文字列](#テンプレート文字列)
        - [タグ付きテンプレートリテラル](#タグ付きテンプレートリテラル)
        - [デフォルト引数](#デフォルト引数)
        - [可変長引数](#可変長引数)
        - [オブジェクトの分割代入](#オブジェクトの分割代入)
        - [オブジェクトの展開](#オブジェクトの展開)
        - [クラス定義](#クラス定義)
        - [シンボル](#シンボル)
        - [末尾のカンマ](#末尾のカンマ)
    - [TypeScriptの機能概要](#typescriptの機能概要)
        - [TypeScriptの概要](#typescriptの概要)
        - [型注釈](#型注釈)
        - [型推論](#型推論)
        - [共有体型](#共有体型)
        - [列挙型](#列挙型)
        - [文字列リテラル型](#文字列リテラル型)
        - [関数型](#関数型)
        - [文字列インデクサ型](#文字列インデクサ型)
        - [型のエイリアス](#型のエイリアス)
        - [クラス定義](#クラス定義-1)
        - [インターフェース定義](#インターフェース定義)
        - [クラス・インターフェースの継承と Abstract](#クラスインターフェースの継承と-abstract)
        - [ジェネリック型](#ジェネリック型)
        - [keyof](#keyof)
        - [タプル](#タプル)

## ES6以上 の主な変更点

### モジュール(import/export)

他のJSファイルで`export`で定義している、クラス、変数、関数などを参照できるようになる。

例:

```js
// module.js
export const EIM_URL = 'https://eim-developers.eim-cloud.com/';
export class EimAPI {
    getDocument(documentId) { 
        //...
    }
}

export default EimAPI;
```

参照方法はいくつかある

```js
// main.js
// モジュール内のすべてを参照する
import * as Module from './module.js';

const url = Module.EIM_URL+'/service/login/';
const eimApi = new Module.EimAPI();
eimApi.getDocument('xxxxxxxxxxxxxxxxx');

// -----------------
// 指定したものだけ参照する。 名前を変更することもできる。
import { EimApi, EIM_URL as baseurl } from './module.js';

const url = baseurl+'/service/login/';
const eimApi = new EimAPI();

// -----------------
// デフォルトの要素を参照する。
import EimAPI from './module.js';

const eimApi = new EimAPI();

```

### 変数と定数

変数の宣言はこれまで`var`しかなかったが、下記が追加された。

* let
    * var の代替。
    * スコープが常識的なものになる。
    * 再宣言ができない。
* const
    * 定数
    * 再割当てができない

これにより、`var`を使う必要はなくなった、というか **使ってはいけない** 。

また、`let`よりも`const`を多く使うことになる場合が多い。

```js
// let のスコープは常識的
let v1 = 'a';
var v2 = 'b';
if (true) {
    let v1 = 0; // 上の v1 とは別のもの
    var v2 = 0; // 上の v2 の再割当て
}
console.log(v1); // "a"
console.log(v2); // 0

// 定数
const name = 'ebihara';
// name = 'ebikatsu'; // エラー
// オブジェクトはそれ自体が置き換わらなければ、オブジェクト内のプロパティの変更は問題ない。
const array = [];
array.push('ebihara'); // エラーにはならない
console.log(array); // Array [ "ebihara" ]
// array = []; // これはエラー
```

### arrow関数

無名関数を簡単に書くことができる。

下記は、 **ほぼ** 同じ

```js
function hoge(a) {
    console.log(a);
}

const hoge = (a) => {
    console.log(a)
}
```

`this` の取り扱いが異なる。

通常の function だと、呼び出し方によって `this` の内容が異なる。

arrow 関数であれば、this は書いたところの `this` に拘束される。

arrow 関数のほうが、わかりやすいので function はできるだけ **使わない** ほうが良い。

```js
class Hoge {
	constructor() {
  	this.age = 10;
    this.getAge = () => {
  		return this.age;
  	} 
  }
 	getAgeFn() {
  	return this.age;
  }
}
function t(callback) {
	console.log(callback());
}
const hoge = new Hoge();
t(hoge.getAge);
// 10
t(hoge.getAgeFn);
// exception 'this' is undefined
```

### テンプレート文字列

文字列リテラルとして、`"ダブルクォーテーション"` と `'シングルクォーテーション'` が使われる。

これに加え、`` `バッククォート` ``も文字列リテラルとして利用できるようになったが、これは更に内部で変数を埋め込んで展開できるようになった。

```js
let name = 'ebihara kenji';
let date = new Date();
console.log(`こんにちは! ${name} さん!`);
// こんにちは! ebihara kenji さん!
console.log(`今は ${date.toString()}　です。 `);
// 今は Tue Jun 12 2018 12:58:17 GMT+0900　です。

// 改行もいける
console.log(`今は
${date.toString()}
です。 `);
// 今は
// Wed Jun 13 2018 14:34:43 GMT+0900 (東京 (標準時))
// です。

//　エスケープしないで出力する
console.log(String.raw`改行は\nでできます`);
// 改行は\nでできます
```

最後の記述は、 **タグ付きテンプレートリテラル** と言われるもの

### タグ付きテンプレートリテラル

関数の後にテンプレートリテラルを書くことで、テンプレートリテラルの文字や、${}で指定した値などをその関数で拾えるようになる。

テンプレートリテラルの文字を任意の処理をかけて変換する、例えばHTMLやURLエンコードなどの用途として使える

```js
var a = 5;
var b = 10;

function tag(strings, ...values) {
  console.log(strings[0]); // "Hello "
  console.log(strings[1]); // " world"
  console.log(strings); // " world"
  console.log(values[0]);  // 15
  console.log(values[1]);  // 50

  return "Bazinga!";
}

tag`Hello ${ a + b } world ${ a * b}`;
// "Bazinga!
```

### デフォルト引数

関数の引数の宣言で、引数を指定しない場合のデフォルト値の指定ができるようになった。

```js
const hello = (name, keisho='さん') => {
    console.log(`ようこそ ${name} ${keisho}`);
}
hello('kenji');
// ようこそ kenji さん
```

### 可変長引数

関数の引数で、可変の数の引数を宣言できるようになった。  
ただし、他の引数の最後に指定する必要がある。

```js
const hello = (name, ...keisho) => {
    keisho.forEach(a => {
        console.log(`ようこそ ${name} ${a}`);
    });
}
hello('kenji', 'さん', 'さま', 'くん');
// ようこそ kenji さん
// ようこそ kenji さま
// ようこそ kenji くん
```

### オブジェクトの分割代入

オブジェクト内のプロパティをそのプロパティと同じ名前のローカル変数で参照する場合、簡単に書くことができる。

```js
const user = {
    name: 'kenji ebihara',
    age: 29,
    address: 'yoshino-cho kagosima-ken',
};
const {name, age} = user;
console.log(name);
// kenji ebihara
console.log(age);
// 29
```

### オブジェクトの展開

オブジェクト内のプロパティを、"..."で同じ名前の変数にすべて展開する。

展開した内容は1階層コピー（いわゆるシャローコピー）で、オブジェクトが含まれる場合、そのオブジェクトはコピー元と同じものが参照される。

```js
const user = {
    name: 'kenji ebihara',
    age: 29,
    address: 'yoshino-cho kagosima-ken',
};

const assignUser = {
    ...user,
    assignData: new Date(),
}
console.log(assignUser);
/*
{
    "name": "kenji ebihara",
    "age": 29,
    "address": "yoshino-cho kagosima-ken",
    "assignData": "2018-06-12T04:39:46.354Z"
}
*/
```
配列の結合にも利用できる。

```js
const abc = ['a', 'b', 'c'];
const a123 = ['1', '2', '3', ...abc];

console.log(a123);
// [ "1", "2", "3", "a", "b", "c" ]
```

### クラス定義

function でコンストラクタ関数として書くことにより、クラスっぽい事はできた。  
しかし、特別な宣言もなく通常の関数との違いがわからないことから、誤って通常関数のように使ってしまうと、エラーになるならまだしも、場合によってはエラーにもならず変な動作をしてしまうこともあり得る。

class ステートメントが追加されたことで、これを利用することが望ましい。

ただしJavaやC#異なり、プロパティはコンストラクタの中で定義する。

```js
class Polygon {
    // height = 0 // こんな事はできない
    // コンストラクタ
    constructor(height, width) {
        this.height = height;
        this.width = width;
    }
  
    get area() {
        return this.calcArea();
    }

  calcArea() {
    return this.height * this.width;
  }
}

let square = new Polygon(10, 10);

console.log(square.area());
// 100
```

### シンボル

新しく登場したプリミティブ型

```js
let sym = Symbol();
console.log(typeof sym);
// "symbol"
```

`Symbol()`で呼び出してできる値は、常に別な値

```js
let sym1 = Symbol();
let sym2 = Symbol();
console.log(sym1 === sym2);
// false
```

オブジェクトのインデクサとして使える

```js
let sym1 = Symbol();
let sym2 = Symbol();
let obj = {
    [sym1]: 'abc',
    [sym2]: 100,
}
console.log(obj[sym1]);
// "abc"
console.log(obj[sym2]);
// 10
// 次は undefined
console.log(obj['sym1']);
// undefined
```

シンボルに名前をつけることができる

```js
let sym1 = Symbol('a');

console.log(sym1.toString());
// "Symbol(a)"

// 同じ名前つけても別な値
let sym2 = Symbol('a');
console.log(sym1 === sym2)
// false
```

作成したシンボルを一元管理する機能もある。  
`Symbol.for` メソッドの引数に文字列を渡すと、やはりシンボルが返る。しかし、過去に同じ引数で渡していたことがあった場合、それと同じ値を返す。

```js
let sym1 = Symbol('a');
let sym2 = Symbol.for('a');
let sym3 = Symbol.for('a'); // sym2 と同じ値が返る
console.log(sym1 === sym2);
// false
console.log(sym2 === sym3);
// true
```

### 末尾のカンマ

オブジェクトリテラルや配列リテラル、関数の引数で、最後の項目の後にカンマが付けられる。

後に要素を足したり、順番を入れ替えたりするときに、カンマを付けたり取ったりする手間が省かれる。

eslint などでは必須のルールとすることができる。

```js
let obj = {
    name: 'ebihara',
    age: 29, // OK
};
let arr = [ 1, 2, 3, ]; // これもOK
let fn = (a, b, c,) { /* ... */ } // 仕様上はOKだが、対応ブラウザが限られる
```

## TypeScriptの機能概要

### TypeScriptの概要

* 静的型(Type)付けできる JavaScript
* 静的型付けをすることで、スペルミス、変数の型の取り違いなど、実行時にしかわからないエラーが、コンパイル時（IDEの支援があればコーディング中）に発見できるので、ミスが起こりにくい。
* 静的型付けをすることで、エディタやIDEが対応していれば、クラス、オブジェクトやそのメンバー、関数の引数などの入力補完などの支援を受けることができる。
* JavaScript(ES6)のスーパーセットであるため、JavaScriptの構文が完全に利用できる。
    * コンパイルオプションや tslint で、JSの仕様を一部制限することができる。例えば、変数の宣言を絶対にする、var を利用しない、any型の宣言をNGとする、など。
    * コンパイルオプションで、ES5,ES3 の出力も可能であり、対象が ES6 に対応していなくても ES6 を意識したコーディングができる。

### 型注釈

* 変数の宣言時に型を指定できる。

```ts
let name: string = '';
let age: number = 0;
let tel: string[] = [];

// 宣言時と型の異なる値の割当をすると、コンパイル時にエラーになる。
age = "10"; // コンパイルエラー
```

* 関数の引数や戻り値にも型を宣言できる。

```ts
const fn = (name: string, age: number): string => {
    return name + age.toString();
    // 下記のように string を返さないコードを書くと、コンパイルエラー
    // return age;
}
// 利用時にも引数の型が違うとコンパイルエラー
fn('ebihara', '10'); // コンパイルエラー
```

### 型推論

* 変数の宣言時に値を割り当てることで、型を明示的に指定しなくても、割り当てた値から自動的に型が決まる。

```ts
let name = 'ebihara'; // name は string型に決定
name = 10; //　コンパイルエラー
```

* 関数の戻り値からでも推論できる。

```ts
const fn = (name: string, age: number) => {
    return name + age.toString();
}
// 戻り型を指定しなくても、　fn の戻り値は string
let hoge = fn('ebihara', 10); // hoge の型は string
```

### 共有体型

* 1つの変数に、複数の型の宣言ができる。
* いずれかの型の値が入る可能性がある場合に利用する。

* 例えば、EIMのフォーム定義内の```form.formFields[].defaultValue```では、値として```string```, ```Array<string>```, ```Object``` のいずれかの型の値が指定できる。  
これを TypeScriptで定義すると、次のように定義できる。

```ts
let defaultValue: string | string[] | Object,
```

* null や undefined も型の1つとして扱うので、変数にそれらが入る可能性があるのであれば指定する必要がある。

```ts
// 下記はコンパイルエラーとなる。宣言して割当がないと、undefinedが入るため。
let name: string; // コンパイルエラー

// エラーが出ないようにするには、下記のようにする
let name1: string = ''; // 初期化する
let name2: string | undefined; // undefined との共有体型にする

// name2 を利用するときには、型チェックが必要
console.log(name2.length); // コンパイルエラー
// 先に型チェックするとコンパイルエラーにならない
if (!!name2) {
    console.log(name2.length);
}
```

### 列挙型

* C# や Java での```enum```が TypeScript でも利用できる。

```ts
enum statusCode {
    creating, // = 0
    editing, // = 1
    browsing, // = 2
}
console.log(statusCode.creating); // 0
```

* 文字列も可能

```ts
enum state {
    creating = '作成中',
    editing = '編集中',
    browsing = '閲覧中',
}
console.log(state.editing); // "編集中"
```

### 文字列リテラル型

* ```string```型の変数に、特定の文字列しか入らないことを定義できる。

```ts
let status: '作成中' | '編集中' | '閲覧中' = '作成中';
status = 'a'; // コンパイルエラー
```

### 関数型

関数も型定義できる。  
ただ、関数ということだけでなく、引数の数や型、戻り値を指定できる。

```ts
const fn: (name: string, age: number): string => {
    // string を return するステートメントでないと コンパイルエラー
}

interface IHoge {
    getUser: (name: string) => IUser; // 型のみの定義 IUser型のオブジェクトを返す保証がされる
}

class Hoge implements IHoge {
    getUser: () => {  // 引数や戻り値が Interface 定義と異なるのでコンパイルエラー
        return '';
    }
}
```

### 文字列インデクサ型

* v["key"] のようにしてメンバにアクセスできる型を宣言できる。JavaのMap型のようなもの。

```ts
const status: {[code: string]: string} = {
    '00': '初期',
    '10': '申請中',
    '20': '承認済',
};
status['30'] = '廃棄';
console.log(status['00']);
// "初期"
```

### 型のエイリアス

上で説明した型定義に名前を付けて管理することができる。

```ts
type InquiryCategory = '質問' | '意見' | '要望' | '障害';
type name = string | (name: string) => string; // string
// インターフェースのような型も指定できる
type User = {
    age: number,
    name: string,
};

const user: User = {
    age: 0,
    name: '',
};
```

### クラス定義

* ES6 のクラス定義では、プロパティの定義はコンストラクタの中で行うが、TypeScriptでは Java や C# と同じように宣言できる。
* もちろん、プロパティやメソッドに型指定ができる。
* コンストラクタは、`constructor`メソッドで書く。アロー関数で書くことはできない。
* アクセス修飾子 public, private, protected が指定できる。デフォルトは public。
* readonly パラメータプロパティを付けることで、読み取り専用のプロパティを定義できる。

```ts
class User {
    familyName = '';
    givenName = '';
    age: number | null = null;
    gender: 'male' | 'female' | null = null;
    constructor() {
        // 初期化処理
    }
    public getDisplayName = () => {
        return `${this.familyName} ${this.givenName}`;
    }
    private lastAccessDate: Date | null = null; // 外からアクセスできない
    public setLastAccessDate = (date: Date) => { // 明示的に公開
        this.lastAccessDate = date;
    }
    public readonly authority: string; // 読み取り専用で公開
}
```

### インターフェース定義

* 一般的なオブジェクト指向言語のように、インターフェースが定義できる。

```ts
interface IUser {
    familyName: string,
    givenName: string,
    age: number,
    gender: 'male' | 'female' | null,
    getDisplayName: () => string;
}
```

* 実装クラスを作るには、 `implements` する。
* きちんと型どおりに、もれなく定義しないとコンパイルエラーとなる。

```ts
class User implements IUser {
    familyName = '';
    givenName = '';
    age = -1;
    gender = null,
    getDisplayName = () => {
        return `${this.familyName} ${this.givenName}`;
    };
}
const user = New User();
```

* オブジェクトリテラルを使えば、クラスは要らない。
* メンバーが不足していたり、型が異なれば、コンパイルエラー

```ts
const user: IUser = {
    familyName: '',
    givenName: '',
    age: -1,
    gender: null,
    getDisplayName: () => {
        return `${this.familyName} ${this.givenName}`;
    },
};
```

### クラス・インターフェースの継承と Abstract

* クラス・インターフェースは、`extends`でそれぞれ他のものから継承できる。

```ts
interface IUser {
    familyName: string;
    givenName: string;
    age: number | null;
    gender: 'male' | 'female' | null;
}

interface IStaff extends IUser {
    org: string;
}

class User implements IUser {
    familyName = '';
    givenName = '';
    age = null;
    gender = null;
}

class Staff extends User {　// Userの
    org = '';
}
// または
class Staff extends User implements IStaff {
    org = '';
}
```

* `abstract`を付けることで、継承されることを必須にする

```ts
abstract class User implements IUser {
    familyName = '';
    givenName = '';
    age = null;
    gender = null;
    abstract getDisplayName: () => string; // 継承されることが前提なので、実装はない
}
// 継承しないと new できない
// let user = new User(); // コンパイルエラー
class Staff extends User {
    getDisplayName: () => { // abstract がついているので、実装しないとコンパイルエラー
        reutnr '';
    }
}
```

### ジェネリック型

* ジェネリック型も利用可能。
* わかりやすいのが Array。型の宣言には必ず内部の型が必要。

```ts
let user = new User; // IUser インターフェースを継承したクラス
// ジェネリック型はプリミティブ型でも、クラスでも、インターフェースでも良い
const users: Array<IUser> = [];
users.push(user);
// users.push('user'); // コンパイルエラー 別な型は入らない
```

* クラスで、管理する特定のプロパティが外部で指定される場合
* 例えば、EIMで文書を管理するインターフェースを定義しようとした場合  
  systemは統一だが、properties の内容は文書によって異なる。  
  インターフェースの継承でも良いが、ジェネリック型を使うこともできる。
```ts
interface EimDocument<T> {
    system : ISystem, // 別なところで定義してあるつもり
    property: T, // 利用する際に指定した型のプロパティとなる
}
interface IUserPros {
    familyName: string;
    givenName: string;
    age: number | null;
    gender: 'male' | 'female' | null;
}
const getEimDocument = <P>(docId: string): EimDocument<P> => {
    let json = '';
    // ファイルとか、httpとかでJSONを取ってくる処理
    return JSON.parse(json) as EimDocument<P>; // any 型から変換
}
let userDoc = getEimDocument<IUserProps>(docId);
let fname = userDoc.property.familyName; // このようにアクセスできる
```

* ジェネリック型として渡す型を、ある型から継承したもののみに限定したい場合

```ts
const sendUser = <T extends IUser>(user: T, onSuccess: (user: T) => void) => {
    // Tは、IUserを継承しているので、そのメンバーを利用した処理ができる。
    // コールバック関数を実行
    onSuccess(user);
};
interface IStaff extends IUser {
    // 拡張
    org: string;
}
let staff: IStaff = {
    // オブジェクトの作成
};
sendUser<IStaff>(staff, (s) => {
    // s は Staff 型であることを定義しているので、キャストすることなく利用できる。
    console.log(s.org); // 問題なくアクセスできる。
}) ;

```

### keyof

* keyof を使うと、オブジェクトやクラス、インターフェースのメンバーから、ストリングリテラル型を生成できる。

```ts
interface MyInterface {
    a: string;
    b: number;
    c: boolean;
}
type AKeys = keyof MyInterface;
// AKeys = 'a' | 'b' | 'c'
// 上の ReacOnly の型を作りたい
type MyInterfaceClone = {
  readonly [P in keyif MyInterface]: MyInterface[P]
};
// この下と同等
// MyInterfaceClone = {
//  readonly a: string;
//  readonly b: number;
//  readonly c: boolean;
// }
// もっと簡単に同じもの
```

* 既存のクラスから、全て async なメソッドのものや、null許容・不許容なものを新しく作ることなどが用意になる。

### タプル

タプルもあるけど、殆ど使わないだろうから説明しない。

<!--
ts

    Promise関係
        async
        await

    型判定

    import JSON による型推論
 -->

